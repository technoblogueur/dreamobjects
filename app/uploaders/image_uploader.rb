# encoding: utf-8

class ImageUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick


  # interesting link if you want to move files from directory to another using CarrierWave
  # http://stackoverflow.com/questions/15823061/how-can-i-reorganize-an-existing-folder-hierarchy-with-carrierwave


  include CarrierWave::MimeTypes
  process :set_content_type

  # Choose what kind of storage to use for this uploader:
  storage :fog
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
      # http://stackoverflow.com/questions/38175198/carrierwave-wrong-image-url-after-save/38175382#38175382
      # when I save the image I override the store_id and save the uid in the database
      # when you retrieve the record carrierwave use this store_id to make the image_url
      # so model.uid will do the fix
    "uploads/#{model.uid}" # "uploads/#{model.class.to_s.underscore}/#{mounted_as}/test"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  # version :thumb do
  #   process :resize_to_fit => [150, 150]
  # end

  process :resize_to_limit => [300, 300] # this avoid uploading the original image - source : http://stackoverflow.com/questions/8579425/how-can-i-make-carrierwave-not-save-the-original-file-after-versions-are-process

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # helped me : http://stackoverflow.com/questions/4443494/renaming-uploaded-files-with-carrierwave
  # see also : https://github.com/carrierwaveuploader/carrierwave/wiki/How-to:-Customize-your-version-file-names
  # to override the file name
  def filename
    "thumb_#{original_filename}" if original_filename
  end

end
